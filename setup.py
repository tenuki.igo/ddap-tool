import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ddrap-tool",
    version="0.0.1",
    author="tenuki",
    author_email="author@example.com",
    description="Tool to create skeleton files, tag files and such for dungeondraft custom assets",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/na",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
