# DungeonDraft Tool

## Quickstart

1) ```ddap init mypack -a```
1) edit the files that are there to create your pack, follow instructions in the image.
1) add and delete assets until your pack is complete
    1) ```ddap add mypack material mymaterialname```
    1) ```ddap delete mypack materal mymaterialname```
    1) etc....
1) ```ddap update mypack```
1) Import the pack into Dungeondraft

Ready to go, just edit the files in place that are created by the tool. 



## The Details
```
usage: ddap [-h] {init,add,update,delete} ...
   
   Dungeondraft custom asset tool
   
   positional arguments:
     {init,add,update,delete}
                           cmd -h
       init                init -h
       add                 add -h
       update              update -h
       delete              delete -h
   
   optional arguments:
     -h, --help            show this help message and exit
```

```
usage: ddap init [-h] [-a] pack

Create a skeleton asset pack

positional arguments:
  pack        name of the pack you are creating

optional arguments:
  -h, --help  show this help message and exit
  -a, --all   init all resources with default example seeds
```
```
usage: ddap add [-h] pack {preview,wall,tileset,texture,light,object,material,path,pattern,portal,terrain} [name]

Add a resource to the pack with default images and json set up

positional arguments:
  pack                  name of the pack you are creating
  {preview,wall,tileset,texture,light,object,material,path,pattern,portal,terrain}
                        the type of component you want to add
  name                  the name of the component you want to add

optional arguments:
  -h, --help            show this help message and exit
```
```
usage: ddap delete [-h] pack {preview,wall,tileset,texture,light,object,material,path,pattern,portal,terrain} [name]

delete an asset from the pack files and json

positional arguments:
  pack                  name of the pack you are deleting from
  {preview,wall,tileset,texture,light,object,material,path,pattern,portal,terrain}
                        the kind of component you want to delete
  name                  the name of the component you want to delete

optional arguments:
  -h, --help            show this help message and exit
```
```
usage: ddap update [-h] kind

Update everything to get it ready for packing

positional arguments:
  kind        name of the component type you want to add

optional arguments:
  -h, --help  show this help message and exit
```